var z = z || {}

$(document).ready(function() {

    z.root = '//godel.ece.vt.edu/visualAttention';
    z.imageRoot = '/static/val2014';

    z.endpoints = {
        'random': ['GET', '/getDataForHit'],
        'image': ['GET', '/image'],
        'question': ['GET', '/question'],
        'answer': ['GET', '/answer'],
        'submit': ['POST', '/submitAssignmentData']
    };

    z.el = {};
    z.data = {};

    z.utils = {

        'imageIdxInHit': 0,
        'maxIdxInHit': 5,
        'numBlurLevels': 4,
        'blurScale': 2.4,
        'brushSize': 70,
        'initialMapBrightness': 30.0,
        'hiddenFields': ['hitId','assignmentId','workerId'],
        'mode': 'preview',
        'sandbox': true,
        'amtSubmit': false,
        'maxWidth': 400,
        'maxHeight': 400,
        'interfaces': [1,2,3],
        'interfaceId': -1,

        getRequestParams: function(idx, param) {
            if (typeof(idx) === 'undefined')
                return [];
            else {
                if (typeof(param) === 'undefined')
                    param = '';
                else
                    param = '/' + param;
                return {
                    'type': z.endpoints[idx][0],
                    'url': z.root + z.endpoints[idx][1] + param
                }
            }
        },

        getUrlParameter: function(param) {
            var pageURL = decodeURIComponent(window.location.search.substring(1)),
                urlVariables = pageURL.split('&'),
                parameterName,
                i;

            for (i = 0; i < urlVariables.length; i++) {
                parameterName = urlVariables[i].split('=');

                if (parameterName[0] === param) {
                    return parameterName[1] === undefined ? true : parameterName[1];
                }
            }
        },

        setMode: function(mode) {
            z.utils.mode = mode || 'production';
            if (z.utils.mode == 'preview')
                z.views.renderPreviewMode();
        },

        setSandbox: function() {
            z.utils.sandbox = true;

            var param = z.utils.getUrlParameter('turkSubmitTo');
            if (typeof(param) !== 'undefined') {
                z.utils.sandbox = false;
                if (param.indexOf('sandbox') > -1)
                    z.utils.sandbox = true;
                z.utils.amtSubmit = true;
                $('#amtFormWrap').attr('action', param + '/mturk/externalSubmit')
            }
            console.log('sandbox:' + z.utils.sandbox);
        },

        populateHiddenFields: function() {
            for (var i in z.utils.hiddenFields) {
                var param = z.utils.getUrlParameter(z.utils.hiddenFields[i]);
                if (typeof(param) !== 'undefined')
                    $('#'+z.utils.hiddenFields[i]).val(param);
            }
        },

        blurImages: function()
        {
            var blurSize = z.utils.blurScale;

            z.utils.blurImage(0, 0.0);

            for (var i = 1; i < z.utils.numBlurLevels; i++) {
                z.utils.blurImage(i, blurSize);
                blurSize *= z.utils.blurScale;
            }
        },

        blurImage: function (idx, blurSize) {
            var w = z.el.canvas.width;
            var h = z.el.canvas.height;

            var mapData = z.el.mapcontext.getImageData(0, 0, w, h);
            var imageData = z.el.context.getImageData(0, 0, w, h);
            var r = 0, c = 0;

            var rad = Math.floor(blurSize * 2.0) + 1;
            var gau = Array(2 * rad + 1);
            for (r = -rad; r <= rad; r++)
                gau[r + rad] = Math.exp(-(r * r) / (0.25*2.0 * rad * rad));

            var blurTmp = Array(w * h * 3);

            for (r = 0; r < h; r++) {
                for (c = 0; c < w; c++) {
                    var denom = 0.0;
                    var red = 0.0;
                    var green = 0;
                    var blue = 0.0;
                    var cd, rd;

                    for (rd = -rad; rd <= rad; rd++)
                    if (r + rd >= 0 && r + rd < h) {
                        var weight = gau[rd + rad];
                        var offset = ((r + rd) * w + c) * 4;
                        red += weight * imageData.data[offset];
                        green += weight * imageData.data[offset + 1];
                        blue += weight * imageData.data[offset + 2];
                        denom += weight;
                    }

                    blurTmp[(r * w + c) * 3 + 0] = red / denom;
                    blurTmp[(r * w + c) * 3 + 1] = green / denom;
                    blurTmp[(r * w + c) * 3 + 2] = blue / denom;
                }
            }

            for (r = 0; r < h; r++) {
                for (c = 0; c < w; c++) {
                    var denom = 0.0;
                    var red = 0.0;
                    var green = 0;
                    var blue = 0.0;
                    var cd, rd;

                    for (cd = -rad; cd <= rad; cd++)
                        if (cd + c >= 0 && cd + c < w) {
                            var weight = gau[cd + rad];
                            var offset = ((r) * w + c + cd) * 3;
                            red += weight * blurTmp[offset + 0];
                            green += weight * blurTmp[offset + 1];
                            blue += weight * blurTmp[offset + 2];
                            denom += weight;
                        }

                    z.el.blurImages[idx][(r * w + c) * 3 + 0] = red / denom;
                    z.el.blurImages[idx][(r * w + c) * 3 + 1] = green / denom;
                    z.el.blurImages[idx][(r * w + c) * 3 + 2] = blue / denom;
                }
            }
        },

        createMapData: function() {
            var bl = [];

            var maxBlurRadius = Math.pow(z.utils.blurScale, z.utils.numBlurLevels - 1),
                blurScale = z.utils.blurScale,
                blurRadius = maxBlurRadius;

            for (var i = 1; i < z.utils.numBlurLevels; i++) {
                bl.push(blurRadius);
                blurRadius /= blurScale;
            }

            bl.push(0);

            z.data.mapData = {
                'brushRadius': z.utils.brushSize/2,
                'blurRadiusLevels': bl,
                'imageWidth': z.el.imageObj.width,
                'imageHeight': z.el.imageObj.height,
                'strokes': []
            };
        },

        initStroke: function(ev) {
            z.data.mapData.strokes.push({
                'startTime': new Date().getTime(),
                'endTime': false,
                'points': [{
                    'x': ev._x,
                    'y': ev._y
                }],
                'duration': false
            });
        },

        updateStroke: function(ev) {
            z.data.mapData.strokes[z.data.mapData.strokes.length-1].points.push({
                'x': ev._x,
                'y': ev._y
            });
        },

        endStroke: function() {
            z.data.mapData.strokes[z.data.mapData.strokes.length-1].endTime =
            new Date().getTime();
            z.data.mapData.strokes[z.data.mapData.strokes.length-1].duration =
                z.data.mapData.strokes[z.data.mapData.strokes.length-1].endTime -
                z.data.mapData.strokes[z.data.mapData.strokes.length-1].startTime;
        },

        canvasMouseEvent: function(ev) {
            if (z.utils.mode == 'preview') {
                z.views.enableReplayButton();
                clearInterval(z.utils.mvz._intervalId);
            }

            if (ev.offsetX || ev.offsetX == 0) { // Opera
                ev._x = ev.offsetX;
                ev._y = ev.offsetY;
            } else if (ev.layerX || ev.layerX == 0) { // Firefox
                var target = ev.target || ev.srcElement,
                rect = target.getBoundingClientRect(),
                offsetX = ev.clientX - rect.left,
                offsetY = ev.clientY - rect.top;

                ev._x = offsetX;
                ev._y = offsetY;
            }

            var func = z.el.brush[ev.type];
            if (func) {
                func(ev);
            }
        },

        canvasTouchEvent: function(ev) {
            ev.preventDefault();
            if (z.utils.mode == 'preview') {
                z.views.enableReplayButton();
                clearInterval(z.utils.mvz._intervalId);
            }

            var touches = ev.changedTouches,
                target = ev.target,
                rect = target.getBoundingClientRect();

            for (var i in touches) {
                ev._x = Math.floor(ev.changedTouches[i].clientX - rect.left);
                ev._y = Math.floor(ev.changedTouches[i].clientY - rect.top);

                var func = z.el.brush[ev.type];
                if (func) {
                    func(ev);
                }
            }

        },

        setInterface: function(interfaceId) {
            z.utils.interfaceId = interfaceId || parseInt(z.utils.getUrlParameter('interfaceId'));
            if (z.utils.interfaceId === undefined || z.utils.interfaces.indexOf(z.utils.interfaceId) === -1 )
                z.utils.interfaceId = Math.ceil(Math.random() * z.utils.interfaces.length)
            console.log('interface:' + z.utils.interfaceId);
            // callback(z.utils.interfaceId);
        },

        unblurBrush: function() {
            var brush = this;
            this.started = false;

            this.mousedown = function(ev) {
                z.el.brush.started = true;
                z.el.brush.mouseUp = false;
                z.utils.initStroke(ev);
            };

            this.mousemove = function(ev) {
                if (z.el.brush.started) {
                    z.views.paintMapLocation(ev);
                    z.utils.updateStroke(ev);
                }
            };

            this.mouseup = function(ev) {
                if (z.el.brush.started) {
                    z.el.brush.mousemove(ev);
                    z.el.brush.started = false;
                    z.utils.endStroke(ev);
                    z.el.brush.mouseUp = true;
                }
            };

            this.globalmouseup = function(ev) {
                z.el.brush.mouseUp = true;
            };

            this.mouseenter = function(ev) {
                if (z.el.brush.mouseUp == false)
                    z.el.brush.mousemove(ev);
                else
                    z.el.brush.mouseup(ev);
            };

            this.touchstart = function(ev) {
                z.el.brush.mousedown(ev);
            };

            this.touchmove = function(ev) {
                z.el.brush.mousemove(ev);
            };

            this.touchend = function(ev) {
                z.el.brush.mouseup(ev);
            }
        },

        mapVisualizer: function() {
            var mvz = this;

            this._strokeCount = z.data.mapData.strokes.length;
            this._mask;
            this._imageData = false;
            this._currentStrokeIndex = -1;
            this._currentPointIndex = -1;
            this._done = false;
            this._intervalId = false;

            this.visualize = function() {
                clearInterval(z.utils.mvz._intervalId);

                if (z.data.mapData.strokes.length == 0) return;

                switch(z.utils.interfaceId) {
                    case 1:
                    case 2:
                        z.el.mapcontext.rect(0, 0, z.el.mapcanvas.width, z.el.mapcanvas.height);
                        z.el.mapcontext.fillStyle="black";
                        z.el.mapcontext.fill();
                        z.el.mapcontext.fillStyle="rgb("+z.utils.initialMapBrightness+",0,0)";
                        z.el.mapcontext.fill();
                        z.views.renderBlurredImage();
                        break;
                    case 3:
                        z.views.renderBlurredImage();
                        z.el.mapcontext.drawImage(z.el.imageObj, 0, 0);
                        break;
                }

                z.utils.mask = Array(z.el.imageObj.height * z.el.imageObj.width);
                for (var i = 0; i < z.el.imageObj.height * z.el.imageObj.width; i++)
                    z.utils.mask[i] = z.utils.initialMapBrightness;

                z.utils.mvz._imageData = z.el.mapcontext.getImageData(0, 0, z.el.mapcanvas.width, z.el.mapcanvas.height);

                z.utils.mvz._done = false;
                z.utils.mvz._currentStrokeIndex = 0;
                z.utils.mvz._currentPointIndex = 0;

                z.utils.mvz._mask = Array(z.el.imageObj.height * z.el.imageObj.width);
                for (var i = 0; i < z.el.imageObj.height * z.el.imageObj.width; i++)
                    z.utils.mvz._mask[i] = z.utils.initialMapBrightness;

                z.utils.mvz._intervalId = setInterval(z.utils.mvz.step, 5);
            };

            this.step = function() {
                if (z.utils.mvz._currentStrokeIndex == z.data.mapData.strokes.length) {
                    z.utils.mvz._done = true;
                    clearInterval(z.utils.mvz._intervalId);
                    z.views.enableReplayButton();
                    return;
                }

                if (z.utils.mvz._currentPointIndex == z.data.mapData.strokes[z.utils.mvz._currentStrokeIndex].points.length) {
                    z.utils.mvz._currentStrokeIndex += 1;
                    z.utils.mvz._currentPointIndex = 0;
                    return
                }

                var point = z.data.mapData.strokes[z.utils.mvz._currentStrokeIndex].points[z.utils.mvz._currentPointIndex];

                z.views.paintMapLocation(point, false);

                var w = z.el.canvas.width;
                var h = z.el.canvas.height;

                z.utils.mvz._imageData = z.el.mapcontext.getImageData(0, 0, w, h);
                var r = 0, c = 0;

                var x = Math.round(point.x);
                var y = Math.round(point.y);
                var rs = y - z.utils.brushSize ;
                var re = y + z.utils.brushSize ;
                var cs = x - z.utils.brushSize ;
                var ce = x + z.utils.brushSize ;

                rs = Math.max(0, rs);
                cs = Math.max(0, cs);
                re = Math.min(re, h);
                ce = Math.min(ce, w);

                switch(z.utils.interfaceId) {
                    case 1:
                    case 2:
                        for (r = rs; r < re; r++)
                            for (c = cs; c < ce; c++) {
                                var dist = (r - y) * (r - y) + (c - x) * (c - x);
                                var del = 3.0 * Math.exp(-dist / (0.4 * z.utils.brushSize*z.utils.brushSize));
                                z.utils.mvz._mask[c + r * w] += del;

                                z.utils.mvz._imageData.data[(c + r * w) * 4 + 0] = Math.round(z.utils.mvz._mask[c + r * w]);
                            }

                        z.el.mapcontext.putImageData(z.utils.mvz._imageData, 0, 0);
                        break;
                    case 3:
                        break;
                }
                z.utils.mvz._currentPointIndex += 1;
            }
        }

    };

    z.hooks = {

        requestHitData: function() {

            if (z.utils.imageIdxInHit === z.utils.maxIdxInHit) {
                if (z.utils.amtSubmit == true)
                    $('#amtFormWrap').submit();
                else
                    window.location.href = './hit.html';
            }
            else if (z.utils.imageIdxInHit !== 0) {
                z.hooks.nextImage();
            }
            else {
                var req = z.utils.getRequestParams('random');

                var payload = {
                    'hitId': $('#hitId').val(),
                    'assignmentId': $('#assignmentId').val(),
                    'workerId': $('#workerId').val(),
                    // 'workerId': Math.random(),
                    'isSandbox': z.utils.sandbox
                };

                $.ajax({
                    url: req.url,
                    type: req.type,
                    data: payload,
                    // contentType:"application/json",
                    // dataType:"json",
                    success: function(res) {
                        // console.log(res);
                        if (typeof(res.Error) !== 'undefined') {
                            alert('No more images.');
                        }
                        else {
                            if (z.utils.interfaceId == -1) {
                                // z.utils.setInterface(parseInt(res.hitStyleType));
                                // 2nd interface does best, so we explicitly set that.
                                z.utils.setInterface(2);
                                z.views.showInstructions(z.utils.interfaceId);
                            }
                            z.views.showForm();
                            z.data.uuid = res.uuid;
                            z.data.data = res.data;
                            z.utils.imageIdxInHit += 1;
                            z.views.renderPageNum();
                            z.views.renderImage(res.data[0]);
                            z.views.renderSingleQuestion(res.data[0]);
                        }
                    }
                });
            }
        },

        nextImage: function() {
            z.utils.imageIdxInHit += 1;
            z.views.renderPageNum();
            z.views.renderImage(z.data.data[z.utils.imageIdxInHit-1]);
            z.views.renderSingleQuestion(z.data.data[z.utils.imageIdxInHit-1]);
        },

        requestPreview: function() {
            $.get('preview.json', function(res) {
                z.data.data = res.data;
                z.data.mapData = res.mapData;
                z.views.renderPreviewImage(res.data, true);
                z.views.renderSingleQuestion(res.data);
                z.views.showPreviewForm();
            });
        },

        sendData: function(e) {
            e.preventDefault();
            if ($('#answer').val().trim() == '') {
                $('#answer').focus();
                alert('Please answer the question.');
            }
            else if (z.utils.interfaceId == 1 && $('input[type="radio"]:checked').length == 0) {
                alert('Please mark if you were able to answer the question correctly.');
            }
            else {
                if (z.data.mapData.strokes.length == 0) {
                    var answer = confirm("You haven't deblurred any regions. Are you sure the image doesn't need deblurring to answer the question?")
                    if (answer);
                    else return;
                }

                z.views.showFormLoader();
                var payload = {};
                switch(z.utils.interfaceId) {
                    case 1:
                    case 2:
                        payload = {
                            'questionData': z.data.questionData,
                            'answerData': {
                                'answer': $('#answer').val(),
                                'confidence': $('input[type="radio"]:checked:first').val(),
                                'mapData': z.data.mapData,
                                'mapImage': z.el.mapcanvas.toDataURL().replace('data:image/png;base64,', ''),
                                'blurImage': z.el.canvas.toDataURL().replace('data:image/png;base64,', ''),
                                'timestamp': new Date().getTime()
                            },
                            'hitData': {
                                'hitId': $('#hitId').val(),
                                'assignmentId': $('#assignmentId').val(),
                                'workerId': $('#workerId').val(),
                                'hitComment': $('#comments').val(),
                                'hitDuration': (new Date().getTime() - z.utils.startTime),
                                'uuid': z.data.uuid,
                                'hitStyleType': z.utils.interfaceId,
                                'isLast': z.data.data[z.utils.imageIdxInHit-1].isLast,
                                'isBuffer': z.data.data[z.utils.imageIdxInHit-1].isBuffer,
                                'count': z.data.data[z.utils.imageIdxInHit-1].count,
                                'hitIden': z.data.data[z.utils.imageIdxInHit-1].hitIden
                            }
                        };
                        break;
                    case 3:
                        payload = {
                            'questionData': z.data.questionData,
                            'answerData': {
                                'answer': $('#answer').val(),
                                'confidence': $('input[type="radio"]:checked:first').val(),
                                'mapData': z.data.mapData,
                                'mapImage': z.el.backupmapcanvas.toDataURL().replace('data:image/png;base64,', ''),
                                'blurImage': z.el.canvas.toDataURL().replace('data:image/png;base64,', ''),
                                'timestamp': new Date().getTime()
                            },
                            'hitData': {
                                'hitId': $('#hitId').val(),
                                'assignmentId': $('#assignmentId').val(),
                                'workerId': $('#workerId').val(),
                                'hitComment': $('#comments').val(),
                                'hitDuration': (new Date().getTime() - z.utils.startTime),
                                'uuid': z.data.uuid,
                                'hitStyleType': z.utils.interfaceId,
                                'isLast': z.data.data[z.utils.imageIdxInHit-1].isLast,
                                'isBuffer': z.data.data[z.utils.imageIdxInHit-1].isBuffer,
                                'count': z.data.data[z.utils.imageIdxInHit-1].count,
                                'hitIden': z.data.data[z.utils.imageIdxInHit-1].hitIden
                            }
                        };
                        break;
                }

                var req = z.utils.getRequestParams('submit');

                $.ajax({
                    url: req.url,
                    type: req.type,
                    data: JSON.stringify(payload),
                    contentType:"application/json",
                    dataType:"json",
                    complete: function(res) {
                        z.views.hideFormLoader();
                        z.initialize(z.utils.interfaceId);
                    }
                });

                // console.log(payload);
            }
        }

    };

    z.views = {

        renderImage: function(data) {
            z.el.imageObj = new Image();
            // z.el.imageObj.src = z.root + z.imageRoot + '/' + data.imageName;
            z.el.imageObj.src = z.root + '/static/' + data.imageSubType + '/' + data.imageName;
            // z.el.imageObj.src = 'images/' + data.imageName;
            z.el.imageObj.crossOrigin = "Anonymous";

            z.utils.loadImage = false;

            z.el.imageObj.onload = function() {
                if (z.utils.loadImage == true)
                    return;

                z.el.canvas = document.getElementById('canvas');
                z.el.context = z.el.canvas.getContext('2d');

                z.el.mapcanvas = document.getElementById('mapcanvas');
                z.el.mapcontext = z.el.mapcanvas.getContext('2d');

                z.el.backupmapcanvas = document.getElementById('backupmapcanvas');
                z.el.backupmapcontext = z.el.backupmapcanvas.getContext('2d');

                var w = z.el.imageObj.width,
                    h = z.el.imageObj.height,
                    ar = w/h;

                var w_n = Math.min(w, z.utils.maxWidth),
                    h_n = w_n / ar;

                w = w_n;
                h = h_n;
                ar = w/h;

                h_n = Math.min(h, z.utils.maxHeight);
                w_n = h_n * ar

                z.el.canvas.height = h_n;
                z.el.canvas.width = w_n;

                z.el.context.drawImage(z.el.imageObj, 0, 0, z.el.imageObj.width, z.el.imageObj.height, 0, 0, w_n, h_n);
                z.el.imageObj.src = z.el.canvas.toDataURL();
                z.utils.loadImage = true;

                z.el.imageObj.height = h_n;
                z.el.imageObj.width = w_n;

                z.el.mapcanvas.height = z.el.imageObj.height;
                z.el.mapcanvas.width = z.el.imageObj.width;

                z.el.backupmapcanvas.height = z.el.imageObj.height;
                z.el.backupmapcanvas.width = z.el.imageObj.width;

                z.el.mapcontext.rect(0, 0, z.el.mapcanvas.width, z.el.mapcanvas.height);
                z.el.mapcontext.fillStyle="black";
                z.el.mapcontext.fill();

                z.el.backupmapcontext.rect(0, 0, z.el.backupmapcanvas.width, z.el.backupmapcanvas.height);
                z.el.backupmapcontext.fillStyle="black";
                z.el.backupmapcontext.fill();
                z.el.backupmapcontext.fillStyle="rgb("+z.utils.initialMapBrightness+",0,0)";
                z.el.backupmapcontext.fill();

                z.utils.mask = Array(z.el.imageObj.height * z.el.imageObj.width);
                for (var i = 0; i < z.el.imageObj.height * z.el.imageObj.width; i++)
                    z.utils.mask[i] = z.utils.initialMapBrightness;

                z.el.blurImages = Array(z.utils.numBlurLevels+1);

                for (var i = 0; i <= z.utils.numBlurLevels; i++) {
                    z.el.blurImages[i] = Array(z.el.imageObj.height * z.el.imageObj.width * 3);
                }

                z.utils.blurImages();

                switch(z.utils.interfaceId) {
                    case 1:
                    case 2:
                        z.el.mapcontext.rect(0, 0, z.el.mapcanvas.width, z.el.mapcanvas.height);
                        z.el.mapcontext.fillStyle="rgb("+z.utils.initialMapBrightness+",0,0)";
                        z.el.mapcontext.fill();
                        z.views.renderBlurredImage();
                        break;
                    case 3:
                        z.views.renderBlurredImage();
                        z.el.mapcontext.drawImage(z.el.imageObj, 0, 0, z.el.imageObj.width, z.el.imageObj.height, 0, 0, w_n, h_n);
                        break;
                }

                z.utils.createMapData();

                z.views.showCanvas();
                z.views.showQuestion();
                z.utils.startTime = new Date().getTime();
            };
        },

        renderPreviewImage: function(res, animate) {
            var data = res[0];
            animate = animate || false;

            z.el.imageObj = new Image();
            z.el.imageObj.src = 'images/' + data.imageName;

            z.el.imageObj.onload = function() {
                z.el.canvas = document.getElementById('canvas');
                z.el.context = z.el.canvas.getContext('2d');

                z.el.mapcanvas = document.getElementById('mapcanvas');
                z.el.mapcontext = z.el.mapcanvas.getContext('2d');

                z.el.backupmapcanvas = document.getElementById('backupmapcanvas');
                z.el.backupmapcontext = z.el.backupmapcanvas.getContext('2d');

                z.el.canvas.height = z.el.imageObj.height;
                z.el.canvas.width = z.el.imageObj.width;

                z.el.mapcanvas.height = z.el.imageObj.height;
                z.el.mapcanvas.width = z.el.imageObj.width;

                z.el.backupmapcanvas.height = z.el.imageObj.height;
                z.el.backupmapcanvas.width = z.el.imageObj.width;

                z.el.mapcontext.rect(0, 0, z.el.mapcanvas.width, z.el.mapcanvas.height);
                z.el.mapcontext.fillStyle="black";
                z.el.mapcontext.fill();

                z.el.backupmapcontext.rect(0, 0, z.el.backupmapcanvas.width, z.el.backupmapcanvas.height);
                z.el.backupmapcontext.fillStyle="black";
                z.el.backupmapcontext.fill();
                z.el.backupmapcontext.fillStyle="rgb("+z.utils.initialMapBrightness+",0,0)";
                z.el.backupmapcontext.fill();

                z.el.context.drawImage(z.el.imageObj, 0, 0);

                z.utils.mask = Array(z.el.imageObj.height * z.el.imageObj.width);
                for (var i = 0; i < z.el.imageObj.height * z.el.imageObj.width; i++)
                    z.utils.mask[i] = z.utils.initialMapBrightness;

                z.el.blurImages = Array(z.utils.numBlurLevels+1);

                for (var i = 0; i <= z.utils.numBlurLevels; i++) {
                    z.el.blurImages[i] = Array(z.el.imageObj.height * z.el.imageObj.width * 3);
                }

                z.utils.blurImages();

                switch(z.utils.interfaceId) {
                    case 1:
                    case 2:
                        z.el.mapcontext.rect(0, 0, z.el.mapcanvas.width, z.el.mapcanvas.height);
                        z.el.mapcontext.fillStyle="rgb("+z.utils.initialMapBrightness+",0,0)";
                        z.el.mapcontext.fill();
                        z.views.renderBlurredImage();
                        break;
                    case 3:
                        z.views.renderBlurredImage();
                        z.el.mapcontext.drawImage(z.el.imageObj, 0, 0);
                        break;
                }

                z.utils.mvz = new z.utils.mapVisualizer();

                z.views.showCanvas();
                z.views.showQuestion();
                z.utils.startTime = new Date().getTime();

                if (animate == true) {
                    z.views.visualizeMap()
                    z.views.disableReplayButton()
                }

                if (animate == false) {
                    z.utils.createMapData();
                    z.views.enableReplayButton()
                }
            };
        },

        renderBlurredImage: function(el, context)
        {
            el = el || z.el.canvas;
            context = context || z.el.context;

            var w = el.width;
            var h = el.height;

            var imageData = z.el.context.getImageData(0, 0, w, h);

            var r = 0, c = 0;
            for (r = 0; r < h; r++)
                for (c = 0; c < w; c++) {
                    var idx = z.utils.numBlurLevels - 1;

                    imageData.data[(c + r * w) * 4 + 0] = z.el.blurImages[idx][(c + r * w) * 3 + 0];
                    imageData.data[(c + r * w) * 4 + 1] = z.el.blurImages[idx][(c + r * w) * 3 + 1];
                    imageData.data[(c + r * w) * 4 + 2] = z.el.blurImages[idx][(c + r * w) * 3 + 2];

                }

            context.putImageData(imageData, 0, 0);
        },

        paintMapLocation: function(ev, paintMap) {
            if (typeof(paintMap) == 'undefined')
                paintMap = true;

            var w = z.el.canvas.width;
            var h = z.el.canvas.height;

            var mapData = z.el.mapcontext.getImageData(0, 0, w, h);
            var imageData = z.el.context.getImageData(0, 0, w, h);
            var r = 0, c = 0;

            var x = Math.round(ev._x || ev.x);
            var y = Math.round(ev._y || ev.y);
            var rs = y - z.utils.brushSize ;
            var re = y + z.utils.brushSize ;
            var cs = x - z.utils.brushSize ;
            var ce = x + z.utils.brushSize ;

            rs = Math.max(0, rs);
            cs = Math.max(0, cs);
            re = Math.min(re, h);
            ce = Math.min(ce, w);

            switch(z.utils.interfaceId) {
                case 1:
                case 2:
                    for (r = rs; r < re; r++)
                        for (c = cs; c < ce; c++) {
                            var dist = (r - y) * (r - y) + (c - x) * (c - x);
                            var del = 3.0 * Math.exp(-dist / (0.4 * z.utils.brushSize*z.utils.brushSize));
                            z.utils.mask[c + r * w] += del;

                            if (paintMap)
                                mapData.data[(c + r * w) * 4 + 0] = Math.round(z.utils.mask[c + r * w]);

                            var alpha = 1.0 - ((z.utils.mask[c + r * w] - z.utils.initialMapBrightness) / (255.0 - z.utils.initialMapBrightness));
                            alpha = Math.min(0.999, alpha);
                            alpha = Math.max(0.0, alpha);
                            alpha *= z.utils.numBlurLevels - 1.0;
                            var idx0 = Math.floor(alpha);
                            var idx1 = idx0 + 1;
                            var del = alpha - idx0;

                            imageData.data[(c + r * w) * 4 + 0] = del * z.el.blurImages[idx1][(c + r * w) * 3 + 0] + (1.0 - del) * z.el.blurImages[idx0][(c + r * w) * 3 + 0];
                            imageData.data[(c + r * w) * 4 + 1] = del * z.el.blurImages[idx1][(c + r * w) * 3 + 1] + (1.0 - del) * z.el.blurImages[idx0][(c + r * w) * 3 + 1];
                            imageData.data[(c + r * w) * 4 + 2] = del * z.el.blurImages[idx1][(c + r * w) * 3 + 2] + (1.0 - del) * z.el.blurImages[idx0][(c + r * w) * 3 + 2];

                        }

                    if (paintMap)
                        z.el.mapcontext.putImageData(mapData, 0, 0);
                    z.el.context.putImageData(imageData, 0, 0);
                    break;
                case 3:
                    var backupMapData = z.el.backupmapcontext.getImageData(0, 0, w, h);
                    for (r = rs; r < re; r++)
                        for (c = cs; c < ce; c++) {
                            var dist = (r - y) * (r - y) + (c - x) * (c - x);
                            var del = 3.0 * Math.exp(-dist / (0.4 * z.utils.brushSize*z.utils.brushSize));
                            z.utils.mask[c + r * w] += del;

                            if (paintMap)
                                backupMapData.data[(c + r * w) * 4 + 0] = Math.round(z.utils.mask[c + r * w]);

                            var alpha = 1.0 - ((z.utils.mask[c + r * w] - z.utils.initialMapBrightness) / (255.0 - z.utils.initialMapBrightness));
                            alpha = Math.min(0.999, alpha);
                            alpha = Math.max(0.0, alpha);
                            alpha *= z.utils.numBlurLevels - 1.0;
                            var idx0 = Math.floor(alpha);
                            var idx1 = idx0 + 1;
                            var del = alpha - idx0;

                            imageData.data[(c + r * w) * 4 + 0] = del * z.el.blurImages[idx1][(c + r * w) * 3 + 0] + (1.0 - del) * z.el.blurImages[idx0][(c + r * w) * 3 + 0];
                            imageData.data[(c + r * w) * 4 + 1] = del * z.el.blurImages[idx1][(c + r * w) * 3 + 1] + (1.0 - del) * z.el.blurImages[idx0][(c + r * w) * 3 + 1];
                            imageData.data[(c + r * w) * 4 + 2] = del * z.el.blurImages[idx1][(c + r * w) * 3 + 2] + (1.0 - del) * z.el.blurImages[idx0][(c + r * w) * 3 + 2];
                        }

                    if (paintMap)
                        z.el.backupmapcontext.putImageData(backupMapData, 0, 0);
                    z.el.context.putImageData(imageData, 0, 0);
                    break;
            }

        },

        showInstructions: function(interfaceId) {
            $.get('instructions_' + interfaceId + '.html', function(res) {
                $('#instructions').html(res);
            });
        },

        renderSingleQuestion: function(data) {
            if (z.utils.mode == 'preview') {
                var idx = 0;
                data = data[idx];
            }
            z.data.questionData = data;
            if (z.utils.interfaceId !== 1 && z.utils.mode !== 'preview')
                $('#answer').val(data.mcqAnswer).attr('disabled', 'disabled');
            $('#question').html('Question: ' + data.question);
        },

        showCanvas: function() {
            $('#canvasWrap').show();
        },

        hideCanvas: function() {
            $('#canvasWrap').hide();
        },

        showLoader: function(data) {
            $('.loader').show();
        },

        hideLoader: function(data) {
            $('.loader').hide();
        },

        showQuestion: function() {
            z.views.hideLoader();
            $('#question').show();
        },

        hideQuestion: function() {
            $('#question').hide();
        },

        resetInputFields: function() {
            $('#answer').val('');
            $('input[type="radio"]:checked').removeAttr('checked');
            $('#comments').val('');
        },

        renderPageNum: function() {
            $('#mode').html('');
            $('#page').html(z.utils.imageIdxInHit + '/' + z.utils.maxIdxInHit);
        },

        showForm: function() {
            if (z.utils.interfaceId !== 1)
                $('.confidence-form-el').hide();
            $('.formWrap').show();
        },

        hideForm: function() {
            $('.formWrap').hide();
        },

        showFormLoader: function() {
            $('.formLoader').show();
            $('#submitForm').attr('disabled','disabled');
        },

        hideFormLoader: function() {
            $('.formLoader').hide();
            $('#submitForm').removeAttr('disabled');
        },

        showPreviewForm: function() {
            $('#previewFormWrap').show();
        },

        hidePreviewForm: function() {
            $('#previewFormWrap').hide();
        },

        renderPreviewMode: function() {
            $('#mode').html('[Preview Mode]');
            $('#page').html('');
        },

        visualizeMap: function() {
            z.utils.mvz.visualize();
        },

        enableReplayButton: function() {
            $('#previewButton').removeAttr('disabled');
        },

        disableReplayButton: function() {
            $('#previewButton').attr('disabled', 'disabled');
        },

        enableBlurButton: function() {
            $('#blurButton').removeAttr('disabled');
        },

        blurImage: function() {
            clearInterval(z.utils.mvz._intervalId);
            z.views.renderPreviewImage(z.data.data, false);
        },

        toggleInstructions: function() {
            $('#instructions').toggle();
            $('.instructionsButtonText').toggle();
        },

        markNoAnswer: function() {
            $('input[type="radio"][value="no"]').prop('checked','checked');
        },

        markYesAnswer: function() {
            $('input[type="radio"][value="yes"]').prop('checked','checked');
        },

        markMaybeAnswer: function() {
            $('input[type="radio"][value="maybe"]').prop('checked','checked');
        },

        submitForm: function() {
            $('#submitForm').click();
        },

        reset: function(e) {
            e.preventDefault();
            z.views.renderImage(z.data.data[z.utils.imageIdxInHit-1]);
            z.views.renderSingleQuestion(z.data.data[z.utils.imageIdxInHit-1]);
        }

    };

    z.initialize = function() {
        if (z.utils.getUrlParameter('assignmentId') == 'ASSIGNMENT_ID_NOT_AVAILABLE') {
            z.utils.interfaceId = 2
            z.preview(z.utils.interfaceId);
        }
        else {
            z.utils.setMode('production');
            z.views.showLoader();
            z.views.hideCanvas();
            z.views.hideQuestion()
            z.views.resetInputFields();
            z.utils.populateHiddenFields();
            z.hooks.requestHitData();
            z.views.hidePreviewForm();
        }
    };

    z.preview = function(interfaceId) {
        z.utils.setMode('preview');
        z.views.showInstructions(interfaceId);
        z.views.showLoader();
        z.views.hideCanvas();
        z.views.hideForm();
        z.hooks.requestPreview();
    };

    z.utils.setSandbox();

    z.initialize();

    z.el.brush = new z.utils.unblurBrush();

    if ($('#canvas').length > 0) {
        z.el.canvas = document.getElementById('canvas');
        z.el.context = z.el.canvas.getContext('2d');

        z.el.mapcanvas = document.getElementById('mapcanvas');
        z.el.mapcontext = z.el.mapcanvas.getContext('2d');

        z.el.canvas.addEventListener('mousedown', z.utils.canvasMouseEvent, false);
        z.el.canvas.addEventListener('mousemove', z.utils.canvasMouseEvent, false);
        z.el.canvas.addEventListener('mouseenter', z.utils.canvasMouseEvent, false);
        z.el.canvas.addEventListener('mouseup',   z.utils.canvasMouseEvent, false);
        document.addEventListener('mouseup',   z.el.brush.globalmouseup, false);
        z.el.canvas.addEventListener('touchstart', z.utils.canvasTouchEvent, false);
        z.el.canvas.addEventListener('touchend',   z.utils.canvasTouchEvent, false);
        z.el.canvas.addEventListener('touchmove',  z.utils.canvasTouchEvent, false);
    }


    if ($('#submitForm').length > 0)
        $('#submitForm').click(z.hooks.sendData);

    if ($('#previewButton').length > 0)
        $('#previewButton').click(z.views.visualizeMap);

    if ($('#blurButton').length > 0)
        $('#blurButton').click(z.views.blurImage);

    if ($('#resetButton').length > 0)
        $('#resetButton').click(z.views.reset);

    if ($('#instructionsButton').length > 0)
        $('#instructionsButton').click(z.views.toggleInstructions);

    $('body').hotKey({ key: 'j', modifier: 'ctrl' }, z.views.markYesAnswer);
    $('body').hotKey({ key: 'k', modifier: 'ctrl' }, z.views.markMaybeAnswer);
    $('body').hotKey({ key: 'l', modifier: 'ctrl' }, z.views.markNoAnswer);
    $('body').hotKey({ key: 'return', modifier: 'ctrl' }, z.views.submitForm );

});
