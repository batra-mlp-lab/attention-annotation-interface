var z = z || {}

$(document).ready(function() {

    z.root = '//godel.ece.vt.edu/visualAttention';

    z.uuids = [];

    z.hooks = {

        getReviewableHits: function() {
            $.ajax({
                url: z.root + '/getReviewableHits',
                success: function(res) {
                    z.views.showHits(res.hits);
                }
            });
        }

    };

    z.views = {

        showHits: function(data) {
            var html = $('#hits').html();

            for (var i in data) {
                html += '<div class="row">';
                html += '<div class="col-lg-12">HIT ID: ' + data[i].hitId + '</div>';
                html += '<div class="col-lg-12">Status: ' + data[i].status + '</div>';
                html += '<div class="col-lg-12">Visualization URL: <a href="' + data[i].visualizeHitUrl + '">' + data[i].visualizeHitUrl + '</a></div>';
                html += '<div class="col-lg-12">Manage HIT URL: <a href="' + data[i].manageHitUrl + '">' + data[i].manageHitUrl + '</a></div>';
                html += '</div>';
                html += '<div class="row"><br>';
                html += '<div class="col-lg-12"><button data-uuid="' + data[i].uuid + '" data-hitid="' + data[i].hitId + '" class="btn btn-primary">Approve</button></div>'
                html += '</div>';
                html += '<hr>';
            }

            $('#hits').html(html);
        },

        approveButtonClicked: function(e) {
            var payload = {
                'uuid': $(this).data('uuid'),
                'status': 'approved'
            };

            var btn = $(this);

            $.ajax({
                url: z.root + '/getReviewableHits',
                type: 'POST',
                data: payload,
                success: function() {
                    btn.attr('disabled','disabled');
                    // window.location.href = './';
                }
            });
        }
    };

    z.initialize = function() {
        z.hooks.getReviewableHits();
        $('#hits').on('click', 'button', z.views.approveButtonClicked);
    };

    z.initialize();
});