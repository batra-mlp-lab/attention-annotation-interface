var z = z || {}

$(document).ready(function() {

    z.root = '//godel.ece.vt.edu/visualAttention';

    z.uuids = [];

    z.hooks = {

        getUrlParameter: function(param) {
            var pageURL = decodeURIComponent(window.location.search.substring(1)),
                urlVariables = pageURL.split('&'),
                parameterName,
                i;
            for (i = 0; i < urlVariables.length; i++) {
                parameterName = urlVariables[i].split('=');

                if (parameterName[0] === param) {
                    return parameterName[1] === undefined ? true : parameterName[1];
                }
            }
        },

        getData: function() {
            z.views.updateStatus('Loading');
            var page = z.hooks.getUrlParameter('pageId');
            if (typeof(page) !== 'undefined' && page !== '') {
                z.page = parseInt(page)
            }
            else {
                z.page = 0;
            }
            $.ajax({
                url: z.root + '/getAnnotationsForPage?pageId=' + z.page,
                success: function(res) {
                    z.views.updateStatus('Loaded. Current page = ' + z.page + ' (0-indexed), total pages = ' + res.totalCount);
                    console.log(res.data.length);
                    z.views.showAnnotations(res.data);
                }
            });
        },

        nextPage: function() {
            z.page += 1;
            window.location.href = "./?pageId=" + z.page;
        }

    };

    z.views = {

        showAnnotations: function(data) {
            if (typeof(data) == 'undefined' || data == '')
                return;

            var html = $('#annotations').html();

            for (var i in data) {
                html += '<div class="row">';
                html += '<div class="col-lg-12">Question: ' + data[i].questionData.question + '</div>';
                html += '<div class="col-lg-12">Correct Answer: ' + data[i].questionData.mcqAnswer + '</div>';
                html += '<div class="col-lg-12">Submitted on: ' + (new Date(data[i].answerData.timestamp)) + '</div>';
                html += '<div class="col-lg-12">HIT Interface: ' + data[i].hitData.hitStyleType + '</div>';
                html += '<div class="col-lg-12"><img src="data:image/png;base64,' + data[i].answerData.blurImage + '"><img src="data:image/png;base64,' + data[i].answerData.mapImage + '"></div>';
                html += '<div class="col-lg-12">Entered Answer: ' + data[i].answerData.answer + '</div>';
                html += '<div class="col-lg-12">Confidence: ' + data[i].answerData.confidence + '</div>';
                html += '<div class="col-lg-12">Comments: ' + data[i].hitData.hitComment + '</div>';
                html += '<div class="col-lg-12">Duration: ' + (data[i].hitData.hitDuration * 1.0 / 1000) + 's</div>';
                html += '</div>';
                html += '<hr>';
            }

            $('#annotations').html(html);
        },

        updateStatus: function(message) {
            var html = $('#status').html();
            html += '<li>' + message + '</li>';
            $('#status').html(html);
        }
    };

    z.initialize = function() {
        z.hooks.getData();
    };

    $("#nextButton").click(z.hooks.nextPage);

    z.initialize();
});