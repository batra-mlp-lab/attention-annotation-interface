var z = z || {}

$(document).ready(function() {

    z.root = '//godel.ece.vt.edu/visualAttention';

    z.uuids = [];

    z.hooks = {

        getUrlParameter: function(param) {
            var pageURL = decodeURIComponent(window.location.search.substring(1)),
                urlVariables = pageURL.split('&'),
                parameterName,
                i;

            for (i = 0; i < urlVariables.length; i++) {
                parameterName = urlVariables[i].split('=');

                if (parameterName[0] === param) {
                    return parameterName[1] === undefined ? true : parameterName[1];
                }
            }
        },

        getUuids: function() {
            var uuid = z.hooks.getUrlParameter('uuid');
            if (typeof(uuid) !== 'undefined' && uuid !== '') {
                if (z.uuids.length === 0 || z.uuids[0] !== uuid) {
                    z.uuids.push(uuid);
                    z.hooks.getAnnotations(uuid);
                }
            }
            else {
                if (z.uuids.length !== 0) {
                    var idx = Math.floor(Math.random() * z.uuids.length);
                    z.hooks.getAnnotations(z.uuids[idx]);
                }
                else {
                    $.ajax({
                        url: z.root + '/getAllHitIds',
                        success: function(res) {
                            z.uuids = res.hitIds;
                            var idx = Math.floor(Math.random() * z.uuids.length);
                            z.hooks.getAnnotations(z.uuids[idx]);
                        }
                    });
                }
            }
        },

        getAnnotations: function(uuid) {
            if (typeof(uuid) == 'undefined' || uuid == '')
                return;

            z.views.updateStatus(uuid,'sending request');

            $.ajax({
                url: z.root + '/getAnnotationsForHits',
                data: {
                    uuid: uuid
                },
                success: function(res) {
                    z.views.updateStatus(uuid,'request complete');
                    if (res.data.length == 0) {
                        z.views.updateStatus(uuid,'no data available');
                        z.hooks.getUuids();
                    }
                    else {
                        z.views.updateStatus(uuid,'data received');
                        z.views.showAnnotations(uuid, res.data);
                    }
                },
                error: function(res) {
                    z.views.updateStatus(uuid,'request error');
                }
            });
        }

    };

    z.views = {

        showAnnotations: function(uuid, data) {
            if (typeof(uuid) == 'undefined' || uuid == '')
                return;

            if (typeof(data) == 'undefined' || data == '')
                return;

            var html = $('#annotations').html();

            for (var i in data) {
                html += '<div class="row">';
                html += '<div class="col-lg-12">Question: ' + data[i].questionData.question + '</div>';
                html += '<div class="col-lg-12">Correct Answer: ' + data[i].questionData.mcqAnswer + '</div>';
                html += '<div class="col-lg-12">Submitted on: ' + (new Date(data[i].answerData.timestamp)) + '</div>';
                html += '<div class="col-lg-12">HIT Interface: ' + data[i].hitData.hitStyleType + '</div>';
                html += '<div class="col-lg-12"><img src="data:image/png;base64,' + data[i].answerData.blurImage + '"><img src="data:image/png;base64,' + data[i].answerData.mapImage + '"></div>';
                html += '<div class="col-lg-12">Entered Answer: ' + data[i].answerData.answer + '</div>';
                html += '<div class="col-lg-12">Confidence: ' + data[i].answerData.confidence + '</div>';
                html += '<div class="col-lg-12">Comments: ' + data[i].hitData.hitComment + '</div>';
                html += '<div class="col-lg-12">Duration: ' + (data[i].hitData.hitDuration * 1.0 / 1000) + 's</div>';
                html += '</div>';
                html += '<hr>';
            }

            $('#annotations').html(html);
        },

        updateStatus: function(uuid, message) {
            var html = $('#status').html();
            html += '<li>' + uuid + ': ' + message + '</li>';
            $('#status').html(html);
        }
    };

    z.initialize = function() {
        z.hooks.getUuids();
    };

    z.initialize();
});