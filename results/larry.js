if (window.addEventListener) {
    window.addEventListener('load', function () {
        var canvas,
            mapcanvas,
            viscanvas,
            context,
            mapcontext,
            viscontext,
            imageObj,
            brush,
            brushSize = 60,
            blurRadius = 10,
            images = ['kitten', 'puppy', 'car', 'sepaktakraw', 'pizza'],
            currentImage,
            started = false,
            blurImages,
            numBlurLevels = 5,
            mask;

        var init = function() {
            canvas = document.getElementById('canvas');
            mapcanvas = document.getElementById('mapcanvas');
            
            context = canvas.getContext('2d');
            mapcontext = mapcanvas.getContext('2d');

            viscanvas = document.getElementById('viscanvas');
            viscontext = viscanvas.getContext('2d');
            
            imageObj = new Image();

            if (started == false) {
                currentImage = 'images/' + images[Math.floor(Math.random() * images.length)] + '.jpg';
                started = true;
            }

            imageObj.src = currentImage;

            imageObj.onload = function()
            {
                canvas.height = imageObj.height;
                canvas.width = imageObj.width;

                mapcanvas.height = imageObj.height;
                mapcanvas.width = imageObj.width;

                mapcontext.rect(0,0,mapcanvas.width,mapcanvas.height);
                mapcontext.fillStyle="black";
                mapcontext.fill();

                viscanvas.height = imageObj.height;
                viscanvas.width = imageObj.width;

                viscontext.rect(0,0,viscanvas.width,viscanvas.height);
                viscontext.fillStyle="black";
                viscontext.fill();

                context.drawImage(imageObj, 0, 0);
                
             //   blur(blurRadius, 'canvas');

                mask = Array(imageObj.height * imageObj.width);
                var i;
                for (i = 0; i < imageObj.height * imageObj.width; i++)
                    mask[i] = 0.0;

                blurImages = Array(numBlurLevels);

                for(i=0;i<numBlurLevels;i++)
                {
                    blurImages[i] = Array(imageObj.height * imageObj.width * 3);
                }

                BlurImages();
                InitDisplayImage();

                createMapData();
                mvz = new mapVisualizer();
            };

            brush = new unblurBrush();

            // Event listeners
            canvas.addEventListener('mousedown', canvas_ev_mouse, false);
            canvas.addEventListener('mousemove', canvas_ev_mouse, false);
            canvas.addEventListener('mouseenter', canvas_ev_mouse, false);
            canvas.addEventListener('mouseup',   canvas_ev_mouse, false);
            document.addEventListener('mouseup',   brush.globalmouseup, false);

            canvas.addEventListener('touchstart', canvas_ev_touch, false);
            canvas.addEventListener('touchend',   canvas_ev_touch, false);
            canvas.addEventListener('touchmove',  canvas_ev_touch, false);

            return {
                'blurRadius': blurRadius,
                'brushSize': brushSize
            }
        };

        var BlurImages = function()
        {
            var i;
            blurScale = 2;
            blurSize = blurScale;
            BlurImage(0, 0.0);
            for (i = 1; i < numBlurLevels; i++)
            {
                BlurImage(i, blurSize);
                blurSize *= blurScale;
            }
        }

        var BlurImage = function (idx, blurSize) {
            var w = canvas.width;
            var h = canvas.height;

            var mapData = mapcontext.getImageData(0, 0, w, h);
            var imageData = context.getImageData(0, 0, w, h);
            var r = 0, c = 0;

            var rad = Math.floor(blurSize * 2.0) + 1;
            var gau = Array(2 * rad + 1);
            for (r = -rad; r <= rad; r++)
                gau[r + rad] = Math.exp(-(r * r) / (0.25*2.0 * rad * rad));

            var blurTmp = Array(w * h * 3);

            for (r = 0; r < h; r++) {
                for (c = 0; c < w; c++) {
                    var denom = 0.0;
                    var red = 0.0;
                    var green = 0;
                    var blue = 0.0;
                    var cd, rd;

                    for (rd = -rad; rd <= rad; rd++)
                    if (r + rd >= 0 && r + rd < h) {
                        var weight = gau[rd + rad];
                        var offset = ((r + rd) * w + c) * 4;
                        red += weight * imageData.data[offset];
                        green += weight * imageData.data[offset + 1];
                        blue += weight * imageData.data[offset + 2];
                        denom += weight;
                    }

                    blurTmp[(r * w + c) * 3 + 0] = red / denom;
                    blurTmp[(r * w + c) * 3 + 1] = green / denom;
                    blurTmp[(r * w + c) * 3 + 2] = blue / denom;
                }
            }

            for (r = 0; r < h; r++) {
                for (c = 0; c < w; c++) {
                    var denom = 0.0;
                    var red = 0.0;
                    var green = 0;
                    var blue = 0.0;
                    var cd, rd;

                    for (cd = -rad; cd <= rad; cd++)
                        if (cd + c >= 0 && cd + c < w) {
                            var weight = gau[cd + rad];
                            var offset = ((r) * w + c + cd) * 3;
                            red += weight * blurTmp[offset + 0];
                            green += weight * blurTmp[offset + 1];
                            blue += weight * blurTmp[offset + 2];
                            denom += weight;
                        }

                    blurImages[idx][(r * w + c) * 3 + 0] = red / denom;
                    blurImages[idx][(r * w + c) * 3 + 1] = green / denom;
                    blurImages[idx][(r * w + c) * 3 + 2] = blue / denom;
                }
            }

        }

        var blur = function(blurRadius, canvasID) {
            blurRadius = blurRadius || 5;
            canvasID = canvasID || 'canvas';
            boxBlurCanvasRGB(canvasID, 0, 0, canvas.width, canvas.height, blurRadius, 1);

            return {
                'blurRadius': blurRadius,
                'brushSize': brushSize
            }
        };

        var unblurBrush = function() {
            var brush = this;
            this.started = false;

            this.mousedown = function(ev) {
                brush.started = true;
                brush.mouseUp = false;
                initStroke(ev);
            };

            this.mousemove = function(ev) {
                if (brush.started) {
                    paintMapLocation(ev);
                    updateStroke(ev);
                }
            };

            this.mouseup = function(ev) {
                if (brush.started) {
                    brush.mousemove(ev);
                    brush.started = false;
                    endStroke(ev);
                    brush.mouseUp = true;
                }
            };

            this.globalmouseup = function(ev) {
                brush.mouseUp = true;
            };

            this.mouseenter = function(ev) {
                if (brush.mouseUp == false)
                    brush.mousemove(ev);
                else
                    brush.mouseup(ev);
            };

            this.touchstart = function(ev) {
                brush.mousedown(ev);
            };

            this.touchmove = function(ev) {
                brush.mousemove(ev);
            };

            this.touchend = function(ev) {
                brush.mouseup(ev);
            }
        };

        var InitDisplayImage = function()
        {
            var w = canvas.width;
            var h = canvas.height;

            var imageData = context.getImageData(0, 0, w, h);

            var r = 0, c = 0;
            for (r = 0; r < h; r++)
                for (c = 0; c < w; c++) {
                    var idx = numBlurLevels - 1;

                    imageData.data[(c + r * w) * 4 + 0] = blurImages[idx][(c + r * w) * 3 + 0];
                    imageData.data[(c + r * w) * 4 + 1] = blurImages[idx][(c + r * w) * 3 + 1];
                    imageData.data[(c + r * w) * 4 + 2] = blurImages[idx][(c + r * w) * 3 + 2];

                }

            context.putImageData(imageData, 0, 0);

        }

        var paintMapLocation = function(ev) {
            var w = canvas.width;
            var h = canvas.height;
            
            var mapData = mapcontext.getImageData(0, 0, w, h);
            var imageData = context.getImageData(0, 0, w, h);
            var r = 0, c = 0;

            var x = ev._x;
            var y = ev._y;
            var rs = y - brushSize ;
            var re = y + brushSize ;
            var cs = x - brushSize ;
            var ce = x + brushSize ;

            rs = Math.max(0, rs);
            cs = Math.max(0, cs);
            re = Math.min(re, h);
            ce = Math.min(ce, w);
            
            for (r = rs; r < re; r++)
                for (c = cs; c < ce; c++)
                {
                    var dist = (r - y) * (r - y) + (c - x) * (c - x);
                    var del = 3.0 * Math.exp(-dist / (0.4 * brushSize*brushSize));
                    mask[c + r * w] += del;

                    mapData.data[(c + r * w) * 4 + 0] = Math.round(mask[c + r * w]);
                    var alpha = 1.0 - mask[c + r * w] / 255.0;
                    alpha = Math.min(0.999, alpha);
                    alpha = Math.max(0.0, alpha);
                    alpha *= numBlurLevels - 1.0;
                    var idx0 = Math.floor(alpha);
                    var idx1 = idx0 + 1;
                    var del = alpha - idx0;

                    imageData.data[(c + r * w) * 4 + 0] = del * blurImages[idx1][(c + r * w) * 3 + 0] + (1.0 - del) * blurImages[idx0][(c + r * w) * 3 + 0];
                    imageData.data[(c + r * w) * 4 + 1] = del * blurImages[idx1][(c + r * w) * 3 + 1] + (1.0 - del) * blurImages[idx0][(c + r * w) * 3 + 1];
                    imageData.data[(c + r * w) * 4 + 2] = del * blurImages[idx1][(c + r * w) * 3 + 2] + (1.0 - del) * blurImages[idx0][(c + r * w) * 3 + 2];

                }
                
            mapcontext.putImageData(mapData, 0, 0);
            context.putImageData(imageData, 0, 0);
        }

        // Mouse event handler
        var canvas_ev_mouse = function(ev) {
            if (ev.offsetX || ev.offsetX == 0) { // Opera
                ev._x = ev.offsetX;
                ev._y = ev.offsetY;
            } else if (ev.layerX || ev.layerX == 0) { // Firefox
                var target = ev.target || ev.srcElement,
                rect = target.getBoundingClientRect(),
                offsetX = ev.clientX - rect.left,
                offsetY = ev.clientY - rect.top;
                
                ev._x = offsetX;
                ev._y = offsetY;
            }

            var func = brush[ev.type];
            if (func) {
                func(ev);
            }
        };

        var canvas_ev_touch = function(ev) {
            ev.preventDefault();

            var touches = ev.changedTouches;

            for (var i in touches) {
                ev._x = touches[i].pageX;
                ev._y = touches[i].pageY;

                var func = brush[ev.type];
                if (func) {
                    func(ev);
                }
            }

        };

        var initStroke = function(ev) {
            mapData.strokes.push({
                'startTime': new Date().getTime(),
                'endTime': false,
                'points': [{
                    'x': ev._x,
                    'y': ev._y
                }],
                'duration': false
            });
        };

        var updateStroke = function(ev) {
            mapData.strokes[mapData.strokes.length-1].points.push({
                'x': ev._x,
                'y': ev._y
            });
        };
        
        var endStroke = function() {
            mapData.strokes[mapData.strokes.length-1].endTime = new Date().getTime();
            mapData.strokes[mapData.strokes.length-1].duration = mapData.strokes[mapData.strokes.length-1].endTime - mapData.strokes[mapData.strokes.length-1].startTime;
        };

        var mapVisualizer = function() {
            var mvz = this;
            
            this._strokeCount = mapData.strokes.length;
            this._mask;
            this._imageData = false;;
            this._currentStrokeIndex = -1;
            this._currentPointIndex = -1;
            this._done = false;
            this._intervalId = false;

            this.visualize = function() {
                if (mapData.strokes.length == 0) return;

                mvz._imageData = viscontext.getImageData(0, 0, viscanvas.width, viscanvas.height);

                mvz._done = false;
                mvz._currentStrokeIndex = 0;
                mvz._currentPointIndex = 0;

                mvz._mask = Array(imageObj.height * imageObj.width);
                for (var i = 0; i < imageObj.height * imageObj.width; i++)
                    mvz._mask[i] = 0.0;

                mvz._intervalId = setInterval(mvz.step, 5);
            };

            this.step = function() {
                if (mvz._currentStrokeIndex == mapData.strokes.length) {
                    mvz._done = true;
                    clearInterval(mvz._intervalId);
                    return;
                }

                if (mvz._currentPointIndex == mapData.strokes[mvz._currentStrokeIndex].points.length) {
                    mvz._currentStrokeIndex += 1;
                    mvz._currentPointIndex = 0;
                    return
                }

                var point = mapData.strokes[mvz._currentStrokeIndex].points[mvz._currentPointIndex];

                var w = canvas.width;
                var h = canvas.height;
                
                mvz._imageData = viscontext.getImageData(0, 0, w, h);
                var r = 0, c = 0;

                var x = point.x;
                var y = point.y;
                var rs = y - brushSize ;
                var re = y + brushSize ;
                var cs = x - brushSize ;
                var ce = x + brushSize ;

                rs = Math.max(0, rs);
                cs = Math.max(0, cs);
                re = Math.min(re, h);
                ce = Math.min(ce, w);
                
                for (r = rs; r < re; r++)
                    for (c = cs; c < ce; c++)
                    {
                        var dist = (r - y) * (r - y) + (c - x) * (c - x);
                        var del = 3.0 * Math.exp(-dist / (0.4 * brushSize*brushSize));
                        mvz._mask[c + r * w] += del;

                        mvz._imageData.data[(c + r * w) * 4 + 0] = Math.round(mvz._mask[c + r * w]);
                    }
                    
                viscontext.putImageData(mvz._imageData, 0, 0);
                mvz._currentPointIndex += 1;

            }
        };

        var visualizeMap = function() {
            
            mvz.visualize();
            
        };

        var createMapData = function() {
            var brightnessLevels = [];
            var bl = [];

            var maxBlurRadius = Math.pow(2,numBlurLevels),
                blurScale = 2,
                blurRadius = maxBlurRadius;

            for (var i=0; i<numBlurLevels; i++) {
                brightnessLevels.push((maxBlurRadius - blurRadius)/maxBlurRadius * 255);
                bl.push(blurRadius);
                blurRadius /= blurScale;
            }

            bl.push(0);
            brightnessLevels.push(255);
            
            mapData = {
                'brushRadius': brushSize/2,
                'blurRadiusLevels': bl,
                'brightnessLevels': brightnessLevels,
                'strokes': []
            }
        };

        var getMapData = function() {
            return mapData;
        };

        init();

        window.helpers = {
            blur: blur,
            reset: init,
            visualizeMap: visualizeMap,
            getMapData: getMapData
        }

    }, false);
}